<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" rel="stylesheet" media="screen">
        <link rel="icon" href="/static/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="{{$BasePath}}/static/css/slippry.css"/>
        <link href="{{$BasePath}}/static/style.css" rel="stylesheet" media="screen">
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script> --}}
        <script src="https://code.highcharts.com/highcharts.src.js"></script>

        <script src="https://ankane.github.io/chartkick.js/chartkick.js"></script>
        <title>News Heat @isset($page_title){{$page_title}}@endisset </title>
    </head>
    <body>


        @yield('content')

        <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script type="text/javascript" src="{{$BasePath}}/static/js/slippry.min.js"></script>

        @yield('script')
    </body>
</html>
