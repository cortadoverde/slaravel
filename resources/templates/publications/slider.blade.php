<div class="news-slider-main">

  @include('publications/nota_slider_item', ['article' => $all_articles[0]])

  @foreach($all_articles[0]->cobertura->result->articles AS $publication )
    @include('publications/nota_slider_item', ['article' => $publication])
  @endforeach

</div>
