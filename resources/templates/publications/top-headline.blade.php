@extends('../layout',['page_title' => '/ Publicaciones' ])


@section('content')
  <section class="publications">
    <div class="columns home-columns">

      <div class="column is-two-fifths is-home-left-column">
        @include("publications/slider")
      </div>

      <div class="column is-home-right-column">
        <header id="navbar" class="navbar has-shadow is-spaced">
            <nav class="level total">
              <div class="level-left">
                  <a href="https://www.prosumia.la" class="hide" id="only-logo" target="_blank"><img class="logo" src='/static/only-logo.png'></a>
                  <a href="https://www.prosumia.la" id="link-logo" target="_blank"><img class="logo" src='/static/logo.png'></a>
                  <span id="news-heat-title" class="is-size-5">/ NewsHeat</span>
                  <a href="javascript:void(0)" id="arrow-left" class="hide" >
                  <svg width="25" height="25"
                         xmlns="http://www.w3.org/2000/svg"
                          xmlns:xlink="http://www.w3.org/1999/xlink">
                        <image xlink:href="/static/arrow-left.png" height="25" width="25"></image>
                    </svg>
                    </a>
              </div>
            </nav>
        </header>

        <div class="sliders_container">
          <div class="news-slider">
            @foreach($first_line AS $publication )
              <div class="column is-12-mobile is-4-tablet is-4-desktop">
                @include('publications/nota_slider_item_grid', ['article' => $publication])
              </div>
            @endforeach
          </div>

          <div class="news-slider-last">
            @foreach($second_line AS $publication )
              <div class="column is-12-mobile is-4-tablet is-4-desktop">
                @include('publications/nota_slider_item_grid', ['article' => $publication])
              </div>
            @endforeach
          </div>

        </div>

        {{-- <div class="columns is-multiline">
            @foreach($publications->articles AS $publication )
              <div class="column is-12-mobile is-4-tablet is-4-desktop">
                <div class="new_cover">
                  <figure class="background">
                    <a href="{{$publication->url}}">
                      <img src="{{$publication->urlToImage}}" class="is-slightly-rounded">
                    </a>
                  </figure>
                  <div class="content">
                    <div class="domain_info">
                      <span>
                        <img src="https://www.google.com/s2/favicons?domain={{url_to_domain($publication->url)}}" alt="fav">
                      </span>
                      <span>www.{{url_to_domain($publication->url)}}</span>
                      <span>- Hace {{timeago($publication->publishedAt)}}.</span>
                    </div>
                    <a href="{{$publication->url}}" class="" target="_blank">{{$publication->title}}</a>
                  </div>
                </div>

            </div>
            @endforeach
        </div> --}}

        {{-- @foreach($publications->articles AS $publication )
          <div>
            <h3 class="title is-8">{{$publication->title}}</h3>
          </div>
        @endforeach --}}
      </div>
    </div>
  </section>

@endsection

@section('script')
<script>
    $(function(){
      //
      $('.news-slider').slick({
        arrows:false,
        draggable: true,
        autoplay: false, /* this is the new line */
        autoplaySpeed: 4000,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        touchThreshold: 1000,
        centerMode:true
      })

      $('.news-slider-last').slick({
        arrows:false,
        draggable: true,
        autoplay: false, /* this is the new line */
        autoplaySpeed: 4000,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        touchThreshold: 1000,
        pauseOnHover: false,
        centerMode:true
      })

      var main_options = {
        arrows:false,
        draggable: true,
        autoplay: true, /* this is the new line */
        autoplaySpeed: 4000,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        touchThreshold: 1000,
        pauseOnHover: false,
        centerMode:false,
        dots: true
      };


      $('.news-slider-main').slick(main_options)

      $('.news-slider-main').on('afterChange', function( event, slick, currentSlide ) {
        if( ( $('.news-slider-main .slick-slide').length - 1 ) == currentSlide ) {
          setTimeout(function () {

              $('.news-slider-last').slick('slickNext');
          }, 2000);

        }
      })

      $('.news-slider').on('afterChange', function( event, slick, currentSlide) {
        var noticia = $('.news-slider .slick-active').prev().prev()[0];
        var slider  = $('.main', noticia);

        $('.news-slider-main').slick('unslick');
        $('.news-slider-main').html(slider[0].innerHTML);
        $('.news-slider-main').slick(main_options)
      })

      $('.news-slider-last').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        console.log( currentSlide );
        setTimeout(function () {
            $('.news-slider').slick('slickNext');
            setTimeout(function () {
              $('.news-slider-main').slick('slickGoTo', 0);
              $('.news-slider-main').slick('slickPlay');
            },400);
        }, 400);


      })

    });
</script>
@endsection
