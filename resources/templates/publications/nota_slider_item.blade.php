<div class="new_cover">
  <div class="kenburns-container">
    <img src="{{str_replace('//www.lanacion.com.ar', '', $article->urlToImage) }}" class="is-slightly-rounded">
  </div>
  <div class="content">
    <h2 class="subtitle">
      {{ html_entity_decode( $article->description) }}
    </h2>
    <div class="domain_info">
      <span>
        <img src="https://www.google.com/s2/favicons?domain={{url_to_domain($article->url)}}" alt="fav">
      </span>
      <span>www.{{url_to_domain($article->url)}}</span>
      <span>- Hace {{timeago($article->publishedAt)}}.</span>
    </div>
    <a href="{{$article->url}}" class="" target="_blank">{{$article->title}}</a>
  </div>
</div>
