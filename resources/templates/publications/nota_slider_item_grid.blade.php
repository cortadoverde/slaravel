<div class="new_cover">
  <figure class="background">
    <a href="{{$article->url}}">
      <img src="{{str_replace('//www.lanacion.com.ar', '', $article->urlToImage) }}" class="is-slightly-rounded">
    </a>
  </figure>
  <div class="content">
    <div class="domain_info">
      <span>
        <img src="https://www.google.com/s2/favicons?domain={{url_to_domain($article->url)}}" alt="fav">
      </span>
      <span>www.{{url_to_domain($article->url)}}</span>
      <span>- Hace {{timeago($article->publishedAt)}}.</span>
    </div>
    <a href="{{$article->url}}" class="" target="_blank">{{$article->title}}</a>
  </div>

  <div style="display:none" class="main">
    @include('publications/nota_slider_item', ['article' => $article])

    @foreach($article->cobertura->result->articles AS $publication )
      @include('publications/nota_slider_item', ['article' => $publication])
    @endforeach
  </div>
</div>
