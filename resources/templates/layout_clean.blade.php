<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="/static/favicon.ico" type="image/x-icon" />
        <link href="/static/home.css" rel="stylesheet" media="screen">
        <title>Social Heats</title>
    </head>
    <body>
        @yield('content')

    </body>
</html>