<?php

namespace App;

/**
 * Controlador base
 *
 * define el container de la app
 *
 * define el getter para obtener datos del container
 */
class Controller
{
  private $container;

  public function __construct( $container )
  {
    $this->container = $container;


    if( method_exists( $this , "onConstruct" ) ) {
      call_user_func_array( [ $this, "onConstruct"], [] );
    }

    $this->result = new \StdClass();


    $base_path = dirname( $this->request->getUri()->getBasePath() );

    $this->view->set('BasePath', $base_path);
  }

  public function __get( $value )
  {
    return isset( $this->container[$value] ) ? $this->container[$value] : null;
  }

  public function set( $key, $value )
  {
    $this->container[$key] = $value;
  }
}
