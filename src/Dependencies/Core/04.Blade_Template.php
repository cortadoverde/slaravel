<?php

$container = \App\Loader::getContainer();

$container['view'] = function( $container ){
    return new \Slim\Views\Blade(
        $container['config_render']['template_path'],
        $container['config_render']['template_cache_path']
    );
};
