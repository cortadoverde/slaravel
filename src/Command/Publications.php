<?php
namespace App\Command;

use Illuminate\Database\Capsule\Manager AS DB;

class Publications extends Command
{
    protected $signature = "publications {date=2019-05-04} {limit=100}";

    protected $description = "Actualiza los numeros de comprobantes segun el punto de venta";


    public function handle()
    {
        $date  = $this->argument('date');
        $limit = $this->argument('limit');

        $pending_collect = DB::table('publication')
        ->whereNull('engagement_id')
        ->where('published', ">=", $date . ' 00:00:00')
        ->orderBy('published', 'ASC')
        ->get();

        foreach( $pending_collect AS $publication ) {
            // capturar la url;
            parse_str( $publication->link, $params );

            // Corroborar si existe el link
            $count = DB::table('publication_engagement')
            ->select(DB::raw("COUNT(parsed_link) AS total"))
            ->where('parsed_link', $params['url'])
            ->first();
            if( $count->total == 0 ) {

                // Inicializamos la tabla engagement
                DB::table('publication_engagement')
                        ->insert([
                            'parsed_link' => $params['url'],
                            'comment_count' => 0,
                            'comment_plugin_count' => 0,
                            'reaction_count' => 0,
                            'share_count' => 0,
                            'total_count' => 0
                        ]);
            }

            // Actualizamos el engagement_id 
            DB::table('publication')
                ->where('id', $publication->id)
                ->update(['engagement_id' => $params['url'] ]);
            
            // capturar los datos para la api de shareCount;
            $data = $this->captureData( $params['url'] );
            if( $data !== false ) {

                // Formato de la respuesta:

                // stdClass Object
                // (
                //     [StumbleUpon] => 
                //     [Facebook] => stdClass Object
                //         (
                //             [comment_plugin_count] => 0
                //             [total_count] => 1
                //             [og_object] => 
                //             [comment_count] => 0
                //             [share_count] => 1
                //             [reaction_count] => 0
                //         )

                //     [GooglePlusOne] => 
                //     [Pinterest] => 
                //     [LinkedIn] => 
                // )


                // Formato de la tabla
                // stdClass Object
                // (
                //     [parsed_link] => https://www.eltribuno.com/salta/nota/2018-8-31-0-0-0-la-provincia-asegura-que-hasta-fin-de-ano-las-deudas-estan-cubiertas
                //     [comment_count] => 0
                //     [comment_plugin_count] => 0
                //     [reaction_count] => 0
                //     [share_count] => 1
                //     [total_count] => 1
                // )

                // Insertamos el registro con los datos capturados
                DB::table('publication_engagement')
                    ->where('parsed_link', $params['url'])
                    ->update([
                        'comment_count' => $data->Facebook->comment_count,
                        'comment_plugin_count' => $data->Facebook->comment_plugin_count,
                        'reaction_count' => $data->Facebook->reaction_count,
                        'share_count' => $data->Facebook->share_count,
                        'total_count' => $data->Facebook->total_count
                    ]);
                
                echo "[" . $publication->published . "] Insertando link: " . $params['url'] . "\n"; 
                
            } else {
                echo "[error]\n";
            }

        }

    }

    private function captureData( $link ) 
    {
        $client = new \App\Component\Prosumia\SharedCount();
        try {
            return $client->reactions( $link );
        } catch (\Exception $e) {
            
        }
        return false;
    }


}
