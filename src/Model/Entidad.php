<?php

namespace App\Model;

use Illuminate\Support\Str;

class Entidad extends Model
{
    protected $table = "alert";

    public $incrementing = false;

    public $timestamps = false;

    public function getImagenAttribute()
    {
        return search_imagen($this) ;
    }

}