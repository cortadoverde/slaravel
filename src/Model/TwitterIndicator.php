<?php

namespace App\Model;

class TwitterIndicator extends Model
{
    protected $table = "twitter_indicators";

    public $timestamps = false;

}