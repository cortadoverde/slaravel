<?php

namespace App\Model;

class InstagramIndicator extends Model
{
    protected $table = "instagram_indicators";

    public $timestamps = false;

}