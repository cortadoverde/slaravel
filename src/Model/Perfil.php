<?php

namespace App\Model;

use Illuminate\Support\Str;

class Perfil extends Model
{
    protected $table = "perfiles";


    public function perfiles()
    {
        return $this->belongsToMany(Entidad::class, 'entornos_perfiles', 'entorno_id', 'perfil_id');
    }

    public function entidades()
    {
        return $this->belongsToMany(Entidad::class, 'entidades_perfiles', 'entidad_id', 'perfil_id');
    }


}