<?php

namespace App\Model;

class FacebookIndicator extends Model
{
    protected $table = "facebook_indicators";

    public $timestamps = false;

    public function entidad()
    {
        return $this->belongsTo(Entidad::class);
    }
}