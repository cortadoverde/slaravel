<?php

namespace App\Model;

use Illuminate\Support\Str;

class Panel extends Model
{
    protected $table = "virtual";

    public $incrementing = false;

    public $timestamps = false;
    
    public function entidades()
    {
        return $this->hasMany(Entidad::class, 'virtual', 'id');
    }



}