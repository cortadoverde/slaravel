<?php
session_start();
! defined('DS')      ? define( 'DS', DIRECTORY_SEPARATOR ) : null ;
! defined('ROOT')    ? define( 'ROOT', dirname( __DIR__ ) ) : null ;
! defined('APP_DIR') ? define( 'APP_DIR', ROOT . DS . 'src' ) : null;

$autoload = require_once ROOT . DS . 'vendor' . DS . 'autoload.php';

// date_default_timezone_set('UTC');

$config   = [
  "settings" => [
    "displayErrorDetails" => true
  ]
];

$app = new \Slim\App( $config );

\App\Loader::setApp( $app );

$container = \App\Loader::getContainer();

$container['composer'] = $autoload;

\App\Loader::setPaths([
  'Initial' => [
    'debug' => 'Dependencies/Init'
  ],
  'Config' => [
    'core' => [
      'dir' => [
        'Config/Site',
        'Config/Database'
      ]
    ]
  ],
  'Dependencies' => [
    'core' => 'Dependencies/Core'
  ],
  'Routes' => [
    'common' => 'Routes/Common',
    'admin'  => 'Routes/Admin',
    //'api'    => 'Routes/Api'
  ],
  'Middlewares' => [
    'app' => 'Middleware/src'
  ]
]);

\App\Loader::load();
