<?php

// Login y Logout
$app->get('/login/', App\Controller\Auth::class.":login")->setName('login');
$app->post('/login/', App\Controller\Auth::class.":check_auth")->setName('auth.post.login');
$app->get('/logout/', App\Controller\Auth::class.":logout")->setName('logout');


$app->group('/admin', function($app){


    // Perfiles 
    $app->get('/perfiles/find/', \App\Controller\Admin\Entidades::class. ':search');
    $app->post('/entidades/upload/', \App\Controller\Admin\Entidades::class . ':upload');
    
    
    // Paneles
    $app->get('/paneles/', \App\Controller\Admin\Paneles::class . ":index")->setName('admin.paneles');
    $app->post('/paneles/crear/', \App\Controller\Admin\Paneles::class . ":add");
    $app->get('/paneles/crear/', \App\Controller\Admin\Paneles::class . ":add");
    $app->get('/paneles/editar/{id}/', \App\Controller\Admin\Paneles::class . ":edit");
    $app->post('/paneles/editar/{id}/', \App\Controller\Admin\Paneles::class . ":edit");
    $app->get('/paneles/eliminar/{id}/', \App\Controller\Admin\Paneles::class . ":delete");


    // Entidades
    $app->get('/', \App\Controller\Admin\Entidades::class . ":index")->setName('admin.dashboard');
    $app->get('/entidades/find/', \App\Controller\Admin\Entidades::class . ':search');
    // Editar
    $app->get( '/entidades/editar/{id}/', \App\Controller\Admin\Entidades::class . ':edit');
    $app->post('/entidades/editar/{id}/', \App\Controller\Admin\Entidades::class . ':edit');
    
})->add(new App\Middleware\Auth( $app->getContainer() ));