<?php

namespace App\Controller\Admin;

use App\Model\Entidad;
use App\Model\Panel;

use Illuminate\Database\Capsule\Manager AS DB;

class Entidades extends \App\Controller\Controller
{
    public function index($request, $response)
    {

        $entidades = Entidad::orderBy('term')->get();

        $this->view->set('rows', $entidades);

        return $this->view->render( $response, 'admin/entidades/index' );
    }

    public function search( $request, $response , $args )
    {

      $str = $_GET['str'];

      $entidades = Entidad::where('term', 'like', $str . '%')->where('hide','<>',1)->get();

      return $response->withJson( $entidades->toArray() );
    }

    public function update_field( $request, $response, $args )
    {
        $entidad = Entidad::findOrFail( $args['id'] );

        $value = $request->getParam('value');


        if( DB::schema()->hasColumn($entidad->getTable(), $args['field']) ) {
            $entidad->{$args['field']} = $value;
            $entidad->save();
        }

    }

    public function edit( $request, $response, $args )
    {
      $panels = Panel::all();
      $this->view->set('panels', $panels );

      $primary_key = $args['id'];

      if( $request->isPost()){

        $upload = $request->getUploadedFiles();

        $imagen = $upload['imagen'];


        $_POST['entidad']['hide'] = true;
        if( isset ( $_POST['entidad']['visible'] ) ) {
          $_POST['entidad']['hide'] = false;
          unset($_POST['entidad']['visible']);
        }

        if( $imagen !== null && $imagen->getError() === UPLOAD_ERR_OK ) {
          $filename = pathinfo( $imagen->getClientFilename(), PATHINFO_BASENAME );
          $imagen->moveTo( ROOT . DS . 'public/static/'. $filename );
          $_POST['entidad']['image_file_name'] = $filename;
        }

        try {
          $item = Entidad::findOrFail( $primary_key );

          foreach ($_POST['entidad'] as $key => $value) {
            $item->{$key} = $value;
          }
          $item->save();

        } catch (\Exception $e) {
          debug( $e->getMessage() );
        }
        return $response->withRedirect('/admin');
      } else {
        try {
          $item = Entidad::findOrFail( $primary_key  );
          $this->view->set('panel', $item);
          return $this->view->render( $response, 'admin/entidades/edit' );

        } catch (\Exception $e) {
          debug( $e->getMessage() );
        }

      }

    }


    public function upload( $request, $response )
    {


        $entidad_id = $_POST['id'];

        $imagePath = ROOT . "/public/static/entidades/";

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);

        //Check write Access to Directory

        if(!is_writable($imagePath)){
                $response = Array(
                    "status" => 'error',
                    "message" => 'Can`t upload File; no write Access'
                );
                print json_encode($response);
                return;
            }

        if ( in_array($extension, $allowedExts))
          {
          if ($_FILES["img"]["error"] > 0)
            {
                 $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );
            }
          else
            {

              $filename = $_FILES["img"]["tmp_name"];
              list($width, $height) = getimagesize( $filename );

              move_uploaded_file($filename,  $imagePath . $entidad_id . '.' . $extension);

              $response = array(
                "status" => 'success',
                "url" => "/static/entidades/" . $entidad_id . '.' . $extension,
                "width" => $width,
                "height" => $height
              );

            }
          }
        else
          {
           $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
          }

         print json_encode($response);
            return;

    }
}
