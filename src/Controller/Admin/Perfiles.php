<?php

namespace App\Controller\Admin;

use App\Model\Panel;
use App\Model\Perfil AS Model;

use Illuminate\Database\Capsule\Manager AS DB;

class Perfiles extends \App\Controller\Controller
{

    public function index($request, $response)
    {

        $entidades = Model::get();

        $this->view->set('rows', $entidades);

        return $this->view->render( $response, 'admin/perfiles/index' );
    }

    public function add( $request, $response, $args )
    {
      $item = new Model();
      if( $request->isPost() ) {
        try {

          $upload = $request->getUploadedFiles();   
        
          $imagen = $upload['imagen'];

          if( $imagen->getError() === UPLOAD_ERR_OK ) {
            $filename = pathinfo( $imagen->getClientFilename(), PATHINFO_BASENAME );
            $imagen->moveTo( ROOT . DS . 'public/static/entidades/'. $filename );
            $_POST['perfil']['imagen'] = '/static/entidades/'. $filename;
          }
          
          foreach ($_POST['perfil'] as $key => $value) {
            $item->{$key} = $value;
          }

          $item->save();
          $item->entidades()->sync($_POST['entidades']);
        } catch (\Exception $e) {
          
        }
        return $response->withRedirect('/admin/entidades');
      } else {
        $this->view->set('panel', $item);
        return $this->view->render( $response, 'admin/perfiles/edit' );
      }
    }

    public function edit( $request, $response, $args )
    {
      $primary_key = $args['id'];

      if( $request->isPost()){
      
        $upload = $request->getUploadedFiles();   
        
        $imagen = $upload['imagen'];

        if( $imagen->getError() === UPLOAD_ERR_OK ) {
          $filename = pathinfo( $imagen->getClientFilename(), PATHINFO_BASENAME );
          $imagen->moveTo( ROOT . DS . 'public/static/entidades/'. $filename );
          $_POST['perfil']['imagen'] = '/static/entidades/'. $filename;
        }

        try {
          $item = Model::findOrFail( $primary_key );

          foreach ($_POST['perfil'] as $key => $value) {
            $item->{$key} = $value;
          }
          $item->save();

          $item->entidades()->sync($_POST['entidades']);
        } catch (\Exception $e) {
          
        }
        return $response->withRedirect('/admin/entidades');
      } else {
        $item = Model::with('entidades')->findOrFail( $primary_key  );
        $this->view->set('panel', $item);
        return $this->view->render( $response, 'admin/perfiles/edit' );
      }

    }

    private function updateRecord( $item, $data )
    {
        try {
        
          foreach ($data['perfil'] as $key => $value) {
            $item->{$key} = $value;
          }
          $item->save();

          $item->entidades()->sync($data['entidades']);
        } catch (\Exception $e) {
          
        }
    }

    public function delete( $request, $response, $args )
    {
      $primary_key  = $args['id'];

      try {
        
        $item = Model::findOrFail( $primary_key  );
        $item->entidades()->delete();
        $item->delete();
      } catch (\Exception $e) {
        debug( $e );
      }


      return $response->withRedirect('/admin/entidades');
    }

    public function update_field( $request, $response, $args )
    {
        $entidad = Entidad::findOrFail( $args['id'] );

        $value = $request->getParam('value');


        if( DB::schema()->hasColumn($entidad->getTable(), $args['field']) ) {
            $entidad->{$args['field']} = $value;
            $entidad->save();
        }
        
    }

    public function search( $request, $response , $args )
    {

      $str = $_GET['str'];

      $entidades = Model::where('nombre', 'like', $str . '%')->get();

      return $response->withJson( $entidades->toArray() );
    }

    public function upload( $request, $response )
    {
        
        
        $entidad_id = $_POST['id'];

        $imagePath = ROOT . "/public/static/entidades/";

        $allowedExts = array("gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG");
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);
        
        //Check write Access to Directory

        if(!is_writable($imagePath)){
                $response = Array(
                    "status" => 'error',
                    "message" => 'Can`t upload File; no write Access'
                );
                print json_encode($response);
                return;
            }

        if ( in_array($extension, $allowedExts))
          {
          if ($_FILES["img"]["error"] > 0)
            {
                 $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );          
            }
          else
            {
                
              $filename = $_FILES["img"]["tmp_name"];
              list($width, $height) = getimagesize( $filename );

              move_uploaded_file($filename,  $imagePath . $entidad_id . '.' . $extension);

              $response = array(
                "status" => 'success',
                "url" => "/static/entidades/" . $entidad_id . '.' . $extension,
                "width" => $width,
                "height" => $height
              );
              
            }
          }
        else
          {
           $response = array(
                "status" => 'error',
                "message" => 'something went wrong, most likely file is to large for upload. check upload_max_filesize, post_max_size and memory_limit in you php.ini',
            );
          }
          
         print json_encode($response);
            return;

    }
}