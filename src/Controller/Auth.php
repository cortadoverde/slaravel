<?php

namespace App\Controller;

class Auth extends \App\Controller
{


    public function login( $request, $response, $args )
    {
        return $this->view->render( $response, 'admin/auth/login' );
    }

    public function check_auth( $request, $response )
    {
        $_SESSION['user'] = ['id' => 1];
        return $response->withRedirect( $this->router->pathFor('admin.dashboard') );
    }
}