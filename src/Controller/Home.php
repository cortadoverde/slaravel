<?php

namespace App\Controller;

use App\Model\Entidad;

use Illuminate\Database\Capsule\Manager AS DB;

use Illuminate\Support\Collection;

class Home extends Controller
{

    public function index( $request, $response, $args )
    {
      $cache   = ROOT . DS . 'resources' . DS . 'cache' . DS . 'all_' .date('YmdH')  ;

      $is_cache = false;
      if( ! file_exists( $cache ) ) {
        $newsAPI = new \App\Component\NewsAPI\Client();
        $publications = $newsAPI->all();

        $all_articles = $publications->articles;

        foreach( $all_articles AS $index => $article ) {
          $article->cobertura = $newsAPI->cobertura( $article );
        }
        file_put_contents( $cache, json_encode($all_articles) );
        $all_articles = json_decode ( json_encode($all_articles) ) ;
      } else {
        $all_articles = json_decode( file_get_contents( $cache ) );
        $is_cache = true;
      }


      // debug( $all_articles );
      $first_line   = array_merge( array_slice( $all_articles, 2 ) , array_slice( $all_articles, 0, 2 ) );

      $second_line  = array_merge( array_slice( $all_articles, 5 ) , array_slice( $all_articles, 0, 5 ) );

      // debug($all_articles[0]);
      $this->view->set('all_articles', $all_articles);
      $this->view->set('first_line', $first_line);
      $this->view->set('second_line', $second_line);

      return $this->view->render( $response, 'publications/top-headline' );

    }

}
