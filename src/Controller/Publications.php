<?php

namespace App\Controller;

use App\Model\Entidad;

use Illuminate\Database\Capsule\Manager AS DB;

use Illuminate\Support\Collection;

class Publications extends Controller
{

    public function index( $request, $response, $args )
    {
      $alert_id = $_GET['alert_id'];
      $from     = str_replace('/','', $_GET['from']);
      $to       = str_replace('/','', $_GET['to']);

      $api = new \App\Component\Prosumia\Client();
      $data_set = $api->alerts( $alert_id, $from, $to, 'dateDesc' );

      $this->view->set('data', $data_set);
    
      return $this->view->render( $response, 'home/publications' );
    }

}
