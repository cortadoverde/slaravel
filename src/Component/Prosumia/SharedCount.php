<?php

namespace App\Component\Prosumia;


class SharedCount
{
    private $httpClient;


    private $version = 'v1';


    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client([
            'base_uri' => "https://api.sharedcount.com/v1.0/"
        ]);
    }

    public function reactions( $url )
    {
        $uri = '?apikey=abea846a2cb3ee1841c2b22a9aafddd9bc2f452b&url=' . urlencode( $url );

        $res = $this->httpClient->request('GET', $uri );

        if( $res->getStatusCode() == 401 ) {
            debug( 'Limite quota');            
        }

        return json_decode( $res->getBody()->getContents() );

    }

}