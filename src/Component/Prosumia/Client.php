<?php

namespace App\Component\Prosumia;


class Client
{
    private $httpClient;


    private $version = 'v1';


    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client([
            'base_uri' => "http://maxpower.prosumia.la:5000/v1/"
        ]);
    }

    public function publications( $from, $to, $virtual )
    {
        $uri = "/v1/publications/{$from}/{$to}/{$virtual}";

        $res = $this->httpClient->request('GET', $uri );

        return json_decode( $res->getBody()->getContents() );
    }

    public function alerts( $alert_id, $from, $to, $sort )
    {
        $uri = "/v1/alert/{$alert_id}?from={$from}&to={$to}&sort={$sort}";

        $res = $this->httpClient->request('GET', $uri );

        return json_decode( $res->getBody()->getContents() );

    }

}