<?php

namespace App\Component\Scrapper;


class GoogleNews
{
    private $httpClient;

    private $url = 'https://  /v2/';

    private $api_key = '0d0bad2ca73e421891f128f96628897a';

    private $source = 'google-news-ar';

    private $version = 'v1';


    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client();
        $this->parser     = new \simple_html_dom();
    }

    public function all()
    {
      $uri = "https://news.google.com/topics/CAAqLAgKIiZDQkFTRmdvSUwyMHZNRFZxYUdjU0JtVnpMVFF4T1JvQ1FWSW9BQVAB?hl=es-419&gl=AR&ceid=AR%3Aes-419";
      $res = $this->httpClient->request('GET', $uri );
      $this->parser->load( $res->getBody()->getContents() );

      $titulares = [];
      foreach( $this->parser->find('.NiLAwe.mi8Lec.gAl5If.jVwmLb.Oc0wGc.R7GTQ.keNKEd.j7vNaf.nID9nc') AS $article ) {
        $item = [
          'titulo' => 1
        ];

        $titulares[] = $item;
      }
      debug($titulares);
      return $res->getBody()->getContents();
    }

    public function publications( $from, $to, $virtual )
    {
        $uri = "/v1/publications/{$from}/{$to}/{$virtual}";

        $res = $this->httpClient->request('GET', $uri );

        return json_decode( $res->getBody()->getContents() );
    }

    public function alerts( $alert_id, $from, $to, $sort )
    {
        $uri = "/v1/alert/{$alert_id}?from={$from}&to={$to}&sort={$sort}";

        $res = $this->httpClient->request('GET', $uri, ['connection_timeout'=>5] );

        return json_decode( $res->getBody()->getContents() );

    }

}
