<?php

namespace App\Component\NewsAPI;


class Client
{
    private $httpClient;

    private $url = 'https://newsapi.org/v2/';

    // private $api_key = '0d0bad2ca73e421891f128f96628897a';
    // private $api_key = '80c183a222ec4f0c8d1c26f601f246b2';
    private $api_key = 'dc82c0633dab428091c093abdd4290ee';

    private $source = 'google-news-ar';

    private $version = 'v1';


    public function __construct()
    {
        $this->httpClient = new \GuzzleHttp\Client();
    }

    public function sources()
    {
      $uri = $this->url . 'sources?'
           . implode('&', ['lenguage=es', 'country=ar', 'apiKey=' . $this->api_key])
           ;
      $res = $this->httpClient->request('GET', $uri );

      return json_decode( $res->getBody()->getContents() );
    }

    public function all()
    {
      $uri = $this->url . 'top-headlines' . '?' . implode('&', ['sources='. $this->source, 'apiKey=' . $this->api_key]);
      $res = $this->httpClient->request('GET', $uri );
      $original_body = $res->getBody()->getContents();
      return json_decode( mb_convert_encoding($original_body, 'UTF-8', 'UTF-8') );
    }

    private function commonWord( $array )
    {
      return array_unique( array_diff_assoc( $array, array_unique( $array ) ) );
    }

    public function cobertura( $nota )
    {
      $words = [];

      foreach(['title'] AS $field){
        $words = array_merge(
          $words,
          explode(" ", trim( preg_replace('/[:\\n"]/i', '', $nota->{$field} ) ) )
        );
      }

      // $serchables_words = array_filter(
      //
      //   array_count_values( $words ),
      //
      //   function( $word ) {
      //     return strlen( key( $word ) ) > 3;
      //   }
      // );

      $serchables_words = array_filter(
        array_count_values( $words ),
        function( $word ) {
          return strlen( $word ) > 3;
        },
        ARRAY_FILTER_USE_KEY
      );

      // Ordenar por coincidencias
      arsort( $serchables_words );

      $term = array_keys( array_slice( $serchables_words, 0, 5 ) );
      // $term = array_keys( $serchables_words );

      return $this->search( implode( " OR ", array_map( function( $a ) { return '"'.$a.'"';}, $term ) ) );
      //return $this->search( "Cacerolazo +\"Cristina Kirchner\"" );
    }

    public function search( $term )
    {
      $uri = $this->url . 'everything' . '?' . implode('&', ['apiKey=' . $this->api_key]);
      $sources = [];
      foreach( $this->sources()->sources AS $source ) {
        $sources[] = $source->id;
      }
      $uri .= '&sources=' . urlencode( implode(',', $sources ) );
      $uri .= '&q=' . urlencode( $term );
      $uri .= '&pageSize=5';
      $uri .= '&from='. date('Y-m-d') . 'T00:00:00Z';
      $uri .= '&to='. date('Y-m-d') . 'T23:59:59Z';
      $uri .= '&sortBy=relevancy';
      $res = $this->httpClient->request('GET', $uri );
      return ['term' => $term, 'url' => $uri,  'result' => json_decode( $res->getBody()->getContents() )];
    }

}
