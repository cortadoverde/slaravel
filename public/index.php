<?php

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

! defined('DS')      ? define( 'DS', DIRECTORY_SEPARATOR ) : null ;
! defined('ROOT')    ? define( 'ROOT', dirname( __DIR__ ) ) : null ;
! defined('APP_DIR') ? define( 'APP_DIR', ROOT . DS . 'src' ) : null ;

require APP_DIR . DS .  'bootstrap.php';

$app->run();
